import datetime
import psycopg2
import psycopg2.extras

#TAKING INPUTS       
date_entry = input('From Date')             
year, month, day = map(int, date_entry.split('-'))
date_start = datetime.date(year, month, day)

date_entry2 = input('To Date')
year, month, day = map(int, date_entry2.split('-'))
date_end = datetime.date(year, month, day)

Value = input("Level of Confidence")

#Accessing Database

conn = psycopg2.connect(host = "" , port = 5432, database = "" ,  user =  "" ,  password= "")   #update Credentials
cur = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)

Query = "select sc_date, bacteria_name , antibiotic_name, resistance , sensitivity, sample_type from zevac_zevacmodel where sc_date >%s and sc_date <= %s"

cur.execute(Query,("2013-12-22",date_start,))  #executing query postgresql
results = cur.fetchall()          

cur.execute(Query,(date_start,date_end))    #executing second time to define the interval
results_new = cur.fetchall()

# for row in results:
#     print("Bacteria: {}".format(row['bacteria_name']))
#     print("Antibiotic: {}".format(row['antibiotic_name']))
#     print("sensitivity {}".format(row['sensitivity']))


#5 lists for maintaining
list_count = {}         #counting total 

list_count_sensi = {}   #counting sensitivity
list_sensi ={}          #storing calculated sensitivity

list_count_res = {}     #counting resistivity
list_res= {}            #storing calculated resistivity


def calc(temp):
    for row in temp:
        if (row['bacteria_name']== "Unknown") or (row['antibiotic_name'] == "Unknown"):
            continue 

        key = row['bacteria_name'] +'@' + row['antibiotic_name']

        if (key in list_count): 
            list_count[key] += 1
        else: 
            list_count[key] = 1

        if (key in list_count_sensi): 
            list_count_sensi[key]+= row['sensitivity']
        else:     
            list_count_sensi[key] = row['sensitivity']

        list_sensi[key] = (list_count_sensi[key]/list_count[key]) *100      # for storing sensitivity

        if (key in list_count_res): 
            list_count_res[key] += row['resistance']
        else: 
            list_count_res[key] = row['resistance']

        list_res[key] = (list_count_res[key]/ list_count[key]    )*100      # for storing resistivity


calc(results)
stamp_res= list_res.copy()                      #making copy to compare
stamp_sensi= list_sensi.copy()
calc(results_new)

 
for key in list_sensi:                          #loop to find the combinations not in level of confidence
    if key in stamp_sensi:
            change = float(abs(list_sensi[key] - stamp_sensi[key]))
            if( change> float(Value)):
                print(change)
                Bacteria_name, Antibiotic_name = map(str,key.split('@'))
                print("Bacteria:", Bacteria_name,"||", "Antibiotic:", Antibiotic_name ,"||", list_sensi[key], "|| Change:", change, "||", stamp_sensi[key])
    else:
        Bacteria_name, Antibiotic_name = map(str,key.split('@'))
        print("Bacteria:", Bacteria_name,"||", "Antibiotic:", Antibiotic_name ,"||", list_sensi[key], "|| Change:", list_sensi[key])



    



